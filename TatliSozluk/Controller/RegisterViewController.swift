//
//  RegisterViewController.swift
//  TatliSozluk
//
//  Created by Ali Şengür on 20.07.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth


class RegisterViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var backButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        registerButton.layer.cornerRadius = 6
        backButton.layer.cornerRadius = 6
    }
    
    @IBAction func registerButtonTapped(_ sender: UIButton) {
        
        guard let email = emailTextField.text,
        let password = passwordTextField.text,
            let username = usernameTextField.text
        else { return }
        
        Auth.auth().createUser(withEmail: email, password: password) { (userInfo, error) in
            if let error = error {
                debugPrint("Error occured when creating a new user: \(error.localizedDescription)")
            }
            /// Hata meydana gelmedi o zaman kullanıcı başarılı bir şekilde oluşturuldu
            let changeRequest = userInfo?.user.createProfileChangeRequest() /// Kullanıcı profil verilerini değiştirebilmek için changeRequest nesnesi oluştur.
            changeRequest?.displayName = username
            /// değiştirilen bilgileri commitle
            changeRequest?.commitChanges(completion: { (error) in
                if let error = error {
                    print("Error occured when updating username : \(error.localizedDescription )")
                }
            })
            
            /// hesap oluşturulan kullanıcının userID sini al
            guard let kullaniciAdi = userInfo?.user.uid else { return }
            /// "Kullanıcılar" adında bir koleksiyon oluştur.
            /// Hesabı oluşturulan kullanıcının userID si ile kullanıcıya ait dökümanı oluştur.
            /// setData içerisinde, kullanıcıya ait bilgileri kaydet.
            Firestore.firestore().collection(KULLANICILAR_REF).document(kullaniciAdi).setData([
                KULLANICI_ADI: kullaniciAdi,
                KULLANICI_OLUSTURMA_TARIHI: FieldValue.serverTimestamp()
            ], completion: { (error) in
                if let error = error {
                    debugPrint("Kullanıcı eklenirken hata meydana geldi : \(error.localizedDescription)")
                } else {
                    /// Kullanıcı kayıt işlemi başarılı, dismiss ile "Login" ekranına geri dön.
                    self.dismiss(animated: true, completion: nil)
                }
            })
        }
        
    }
    

    @IBAction func backButtonTapped(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
}
