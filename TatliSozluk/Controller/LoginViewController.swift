//
//  LoginViewController.swift
//  TatliSozluk
//
//  Created by Ali Şengür on 20.07.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth


class LoginViewController: UIViewController {

    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loginButton.layer.cornerRadius = 6
        registerButton.layer.cornerRadius = 6
    }
    

    @IBAction func loginButtonTapped(_ sender: UIButton) {
    
        guard let email = emailTextField.text,
            let password = passwordTextField.text
        else { return }
        
        Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
            if let error = error {
                debugPrint("Error occured when signing in : \(error.localizedDescription)")
            } else {
                self.dismiss(animated: true, completion: nil)
            }
        }
    
    }
    
}
