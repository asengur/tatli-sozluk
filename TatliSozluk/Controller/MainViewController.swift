//
//  MainViewController.swift
//  TatliSozluk
//
//  Created by Ali Şengür on 17.07.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore
import FirebaseAuth


class MainViewController: UIViewController {

    @IBOutlet weak var categorySegmentControl: UISegmentedControl!
    @IBOutlet weak var tableView: UITableView!
    
    
    
    /// Idea tipinde, ideas dizisini oluştur. Firestore'dan çekilen fikirler, bu ideas dizisine eklenecek.
    private var ideas = [Idea]()
    private var ideasCollectionRef: CollectionReference! /// "Fikirler" koleksiyonunun referansını tut.
    private var ideasListener: ListenerRegistration!
    private var selectedCategory = Categories.Eglence.rawValue /// segmentedControl un varsayılan segmenti Eğlence olsun.
    private var listenerHandle: AuthStateDidChangeListenerHandle?
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        categorySegmentControl.backgroundColor = UIColor.white
        categorySegmentControl.selectedSegmentTintColor = #colorLiteral(red: 0.5702994466, green: 0.823569715, blue: 0.9945558906, alpha: 1)
        let titleNormalTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(red: 145/255, green: 210/255, blue: 254/255, alpha: 1)]
        let titleSelectedTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        categorySegmentControl.setTitleTextAttributes(titleNormalTextAttributes, for: .normal)
        categorySegmentControl.setTitleTextAttributes(titleSelectedTextAttributes, for: .selected)
        
        /// "Fikirler" koleksiyonu için referansı oluştur.
        ideasCollectionRef = Firestore.firestore().collection(Fikirler_REF)
        
    }
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        listenerHandle = Auth.auth().addStateDidChangeListener({ (auth, user) in
            if user == nil { /// uygulamaya giriş yapmış bir kullanıcı yoksa "Login" ekranına git.
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                let loginVC = storyboard.instantiateViewController(identifier: "LoginViewController")
                self.present(loginVC, animated: true, completion: nil)
            } else {
                /// Kullanıcı var ise setListener fonksiyonunu çağır.
                self.setListener()
            }
        })
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        if ideasListener != nil { /// ideasListener değişkeni var ise, ekrandan çıkış yapmadan önce bu değişkeni kaldır. Boşu boşuna çalışmasın diye.
            ideasListener.remove()
        }
        
    }
    
    
    
    func setListener() {
        /// Seçilen kategori "Eğlence" ise, tüm fikirleri table view'a yükle.(eklenme tarihine göre en yeni en üstte olacak şekilde "descending = true")
        if selectedCategory == Categories.Populer.rawValue {
            ideasListener = ideasCollectionRef.order(by: Eklenme_Tarihi, descending: true)
                .addSnapshotListener { (snapshot, error) in
                if let error = error {
                    debugPrint("Get document error: \(error.localizedDescription)")
                } else {
                    /// tekrarı önlemek için, "ideas" dizisindeki herşeyi sil.
                    self.ideas.removeAll()
                    /// "Fikirler" koleksiyonundan gelen "snapshot" ı, "Idea" sınıfındaki "getIdeas()" fonksiyonuna gönder. Bu fonksiyon, "ideas "adında bir dizi döndürecek ve bu dizi, bu sınıftaki "ideas" dizisine eklenecek.
                    self.ideas = Idea.getIdea(snapshot: snapshot)
                    self.tableView.reloadData()
                }
            }
        } else { /// Seçilen kategori "Popüler" dışında herhangi bir kategori ise, o kategoriye ait fikirleri getir.
            ideasListener = ideasCollectionRef.whereField(KATEGORI, isEqualTo: selectedCategory)
            .order(by: Eklenme_Tarihi, descending: true)
                .addSnapshotListener { (snapshot, error) in
                if let error = error {
                    debugPrint("Get document error: \(error.localizedDescription)")
                } else {
                    self.ideas.removeAll()
                    self.ideas = Idea.getIdea(snapshot: snapshot)
                    self.tableView.reloadData()
                }
            }
        }
        

    }
    
    
    @IBAction func categorySegmentControlChanged(_ sender: UISegmentedControl) {
        switch categorySegmentControl.selectedSegmentIndex {
        case 0:
            selectedCategory = Categories.Eglence.rawValue
        case 1:
            selectedCategory = Categories.Absurt.rawValue
        case 2:
            selectedCategory = Categories.Gundem.rawValue
        case 3:
            selectedCategory = Categories.Populer.rawValue
        default:
            selectedCategory = Categories.Eglence.rawValue
        }
        ideasListener.remove()
        setListener()
    }
    
    
    

    
    @IBAction func addPostButtonClicked(_ sender: UIBarButtonItem) {
        
        guard let vc = self.storyboard?.instantiateViewController(identifier: "AddPostViewController") as? AddPostViewController else { return }
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    
    
    
    @IBAction func signoutButtonTapped(_ sender: UIBarButtonItem) {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let error as NSError {
            debugPrint("Error occured when signing out: \(error.localizedDescription)")
        }
    }
    
    
}



extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ideas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? IdeaTableViewCell {
            cell.setTableViewCell(idea: ideas[indexPath.row], delegate: self)
            /// "IdeaTableViewCell" türünden bir cell oluştur, daha sonra bu cell ile "setTableViewCell" fonsiyonunu çağır. Bu fonksiyona, "ideas" dizisindeki, ilgili satırın indexinde bulunan fikiri ekle.
            return cell
        } else {
            /// IdeaTableViewCell türünden bir cell yoksa, boş bir cell döndür.
            return UITableViewCell()
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        if let commentsVC = storyboard.instantiateViewController(identifier: "CommentsViewController") as? CommentsViewController {
            /// Satırlara tıklanıldığında, o satıra ait yorumların göründüğü "CommentsViewController" ekranına git.
            /// "CommentsViewController" controller'ındaki "selectedIdea" değişkenina, tıklanan satıraki fikri ekle.
            commentsVC.selectedIdea = ideas[indexPath.row]
            self.navigationController?.pushViewController(commentsVC, animated: true)
        }
    }

    
}






extension MainViewController: IdeaTableViewCellDelegate {
    func ideaOptionsTapped(idea: Idea) {
        print("Seçilen fikir : \(idea.fikirText!)")
    }
}
