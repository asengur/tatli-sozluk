//
//  AddPostViewController.swift
//  TatliSozluk
//
//  Created by Ali Şengür on 17.07.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit
import Firebase
import FirebaseFirestore

class AddPostViewController: UIViewController {

    @IBOutlet weak var categorySegmentControl: UISegmentedControl!
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var postTextView: UITextView!
    @IBOutlet weak var shareButton: UIButton!
    
    
    let placeHolderText = "Fikrinizi Belirtin.."
    var selectedCategory = Categories.Eglence.rawValue
    
    override func viewDidLoad() {
        super.viewDidLoad()

        categorySegmentControl.backgroundColor = UIColor.white
        categorySegmentControl.selectedSegmentTintColor = #colorLiteral(red: 0.5702994466, green: 0.823569715, blue: 0.9945558906, alpha: 1)
        let titleNormalTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor(red: 145/255, green: 210/255, blue: 254/255, alpha: 1)]
        let titleSelectedTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        categorySegmentControl.setTitleTextAttributes(titleNormalTextAttributes, for: .normal)
        categorySegmentControl.setTitleTextAttributes(titleSelectedTextAttributes, for: .selected)
        
        shareButton.layer.cornerRadius = 6
        postTextView.layer.cornerRadius = 6
        
        postTextView.text = placeHolderText
        postTextView.textColor = .lightGray
        
        postTextView.delegate = self
    }
    
    @IBAction func segmentControlChanged(_ sender: UISegmentedControl) {
        
        switch categorySegmentControl.selectedSegmentIndex {
        case 0:
            selectedCategory = Categories.Eglence.rawValue
        case 1:
            selectedCategory = Categories.Absurt.rawValue
        case 2:
            selectedCategory = Categories.Gundem.rawValue
        case 3:
            selectedCategory = Categories.Populer.rawValue
        default:
            selectedCategory = Categories.Eglence.rawValue
        }
        
    }
    
    @IBAction func shareButtonClicked(_ sender: UIButton) {
        
        guard let username = usernameTextField.text else { return }
        
        /// "Fikirler" adında bir koleksiyon oluştur ve bu koleksiyona bir döküman ekle.
        Firestore.firestore().collection(Fikirler_REF).addDocument(data: [
            KATEGORI: selectedCategory, /// segmentedControlden seçilen kategoriyi ekle
            Begeni_Sayisi: 0, /// beğeni ve yorum sayısını başlangıç olarak "0" ekle
            Yorum_Sayisi: 0,
            Fikir_Text: postTextView.text!, /// text view'a girilen fikri ekle
            Eklenme_Tarihi: FieldValue.serverTimestamp(),
            Kullanici_Adi: username,
            KULLANICI_ID: Auth.auth().currentUser?.uid ?? ""
        ]) { (error) in
            if let error = error {
                print("Document Error : \(error.localizedDescription)")
            } else {
                /// Herhangi bir hata yoksa, Ana ekrana geri dön.
                self.navigationController?.popViewController(animated: true)
            }
        }
        
        
    }
    
}



/// Text view için bir extension.
extension AddPostViewController: UITextViewDelegate {
    
    /// Text view aktif olduğunda
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == placeHolderText {
            /// text view in text ini kaldır ve text color dark gray olsun
            textView.text = ""
            textView.textColor = .darkGray
        }
    }
    
    
    /// text view pasif hale geldiğinde
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            /// text view in içi boş ise, placeholder ekle ve text color tekrar light gray olsun.
            textView.text = placeHolderText
            textView.textColor = .lightGray
        }
    }
    
}
