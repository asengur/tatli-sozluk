//
//  CommentsViewController.swift
//  TatliSozluk
//
//  Created by Ali Şengür on 20.07.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth


class CommentsViewController: UIViewController {

    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addCommentTextField: UITextField!
    
    var selectedIdea: Idea! /// Yorumlar ekranında, seçilen yorum bu değişkene atanır.
    var comments = [Comment]() /// Boş bir comments dizisi oluştur.
    var ideaRef: DocumentReference!
    var username: String! /// yorum yapan kullanıcının adını tutmak için username değişkeni oluştur.
    var commentsListener: ListenerRegistration!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        tableView.delegate = self
        tableView.dataSource = self
        
        ideaRef = Firestore.firestore().collection(Fikirler_REF).document(selectedIdea.documentId)
        
        if let name = Auth.auth().currentUser?.displayName {
            username = name
        }
        
        self.view.adjustKeyboard() /// klavye açıldığında text field ın kaybolmaması için
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        /// "Fikirler" koleksiyonuna gir,
        /// Seçilen fikrin döküman ID sini kulanarak o fikrin dökümanına git,
        /// O döküman altındaki "Yorumlar" koleksiyonuna git ve tüm yorumları snapshot olarak çek.
        commentsListener = Firestore.firestore().collection(Fikirler_REF).document(selectedIdea.documentId).collection(YORUMLAR_REF)
            .order(by: Eklenme_Tarihi, descending: false)
            .addSnapshotListener({(snapshot, error) in
                guard let snapshot = snapshot else {
                    debugPrint("Yorumları getirirken hata oluştu : \(error?.localizedDescription)")
                    return
                } /// snapshot var ise;
                self.comments.removeAll()
                /// snapshot'ı Comment classının altındaki getComments() fonksiyonuna gönder. Bu fonksiyon, tüm yorumları bir dizi olarak döndürecek.
                self.comments = Comment.getComments(snapshot: snapshot)
                self.tableView.reloadData() 
            })
        
    }
    
    
    
    
    
    
    @IBAction func addCommentButtonTapped(_ sender: UIButton) {
        guard let commentText = addCommentTextField.text else { return }
        
        /// Yorum eklerken transaction çalışacak. Bu sayede hem yorum eklenecek, hem de yorum sayısı bir arttırılarak güncellenecek.
        Firestore.firestore().runTransaction({(transaction, errorPointer) -> Any? in
            
            let selectedIdeaRecord: DocumentSnapshot
            do {
                /// "Fikirler" koleksiyonundan seçilen fikri al ve "selectedIdeaRecord" değişkenine ata.
                try selectedIdeaRecord = transaction.getDocument(Firestore.firestore().collection(Fikirler_REF).document(self.selectedIdea.documentId))
            } catch let error as NSError{
                debugPrint("Hata meydana geldi : \(error.localizedDescription)")
                return nil
            }
            
            
            /// seçilen fikrin yorum sayısına ulaş ve "commentCountOld" değişkenine ata.
            guard let commentCountOld = (selectedIdeaRecord.data()?[Yorum_Sayisi] as? Int) else { return nil }
            
            /// transaction kullanarak, eski yorum sayısını bir arttır. (Bu işlemi seçilen fikir için yap.)
            transaction.updateData([Yorum_Sayisi : commentCountOld + 1], forDocument: self.ideaRef)
            
            /// yeni eklenecek yorumun referansını al.
            let newCommentRef = Firestore.firestore().collection(Fikirler_REF).document(self.selectedIdea.documentId).collection(YORUMLAR_REF).document()
            
            /// text field' a girilen yorumu, yorumun eklenme tarihini ve yorumu ekleyen kullanıcı adını, yeni eklenecek yorumun referansı ile ekle.
            transaction.setData([
                YORUM_TEXT: commentText,
                Eklenme_Tarihi: FieldValue.serverTimestamp(),
                Kullanici_Adi: self.username,
                KULLANICI_ID: Auth.auth().currentUser?.uid ?? ""
            ], forDocument: newCommentRef)
             
            return nil
        }) {(object, error) in
            if let error = error {
                debugPrint("Error occured in transaction : \(error.localizedDescription)")
            } else {
                self.addCommentTextField.text = ""
            }
        }
    }

}


extension CommentsViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        comments.count 
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "CommentCell", for: indexPath) as? CommentTableViewCell {
            cell.setTableViewCell(comment: comments[indexPath.row], delegate: self)
            return cell
        }
        return UITableViewCell()
    }

}



extension CommentsViewController: CommentTableViewCellDelegate {
    func commentOptionsImageTapped(comment: Comment) {
        let alert = UIAlertController(title: "Editing Comment", message: "You can edit or delete this comment.", preferredStyle: .actionSheet)
        let deleteAction = UIAlertAction(title: "Delete comment", style: .default) { (action) in
            
            
            Firestore.firestore().runTransaction({ (transaction, error) -> Any? in
                let selectedIdeaRecord: DocumentSnapshot
                
                do {
                    try selectedIdeaRecord = transaction.getDocument(Firestore.firestore().collection(Fikirler_REF).document(self.selectedIdea.documentId))
                } catch let error as NSError {
                    debugPrint("Idea can not found : \(error.localizedDescription) ")
                    return nil
                }
                
                
                guard let commentCountOld = (selectedIdeaRecord.data()?[Yorum_Sayisi] as? Int) else { return nil }
                transaction.updateData([Yorum_Sayisi: commentCountOld - 1], forDocument: self.ideaRef)
                
                let deletingCommentRef = Firestore.firestore().collection(Fikirler_REF).document(self.selectedIdea.documentId).collection(YORUMLAR_REF).document(comment.documentId)
                transaction.deleteDocument(deletingCommentRef)
                return nil
            }) {(object, error) in
                if let error = error {
                    debugPrint("Error occured when deleting comment : \(error.localizedDescription)")
                } else {
                    alert.dismiss(animated: true, completion: nil)
                }
            }
        
        }
        let editAction = UIAlertAction(title: "Edit comment", style: .default) { (action) in
            // edit comment
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(deleteAction)
        alert.addAction(editAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    
}
