//
//  KeyboardAdjusting.swift
//  
//
//  Created by Ali Şengür on 24.07.2020.
//

import Foundation
import UIKit



extension UIView {

    func adjustKeyboard() {
        NotificationCenter.default.addObserver(self, selector: #selector(adjustKeyboardPosition(_ :)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    
    @objc private func adjustKeyboardPosition(_ notification: NSNotification) {
        let duration = notification.userInfo![UIResponder.keyboardAnimationDurationUserInfoKey] as! Double
        let curve = notification.userInfo![UIResponder.keyboardAnimationCurveUserInfoKey] as! UInt
        let beginningFrame = (notification.userInfo![UIResponder.keyboardFrameBeginUserInfoKey] as! NSValue).cgRectValue
        let endingFrame = (notification.userInfo![UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue
        let diffY = endingFrame.origin.y - beginningFrame.origin.y
        UIView.animateKeyframes(withDuration: duration, delay: 0.0, options: UIView.KeyframeAnimationOptions.init(rawValue: curve), animations: {
            self.frame.origin.y += diffY
        }, completion: nil )
    }
}
