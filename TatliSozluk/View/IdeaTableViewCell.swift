//
//  IdeaTableViewCell.swift
//  TatliSozluk
//
//  Created by Ali Şengür on 18.07.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit
import Firebase




protocol IdeaTableViewCellDelegate {
    func ideaOptionsTapped(idea: Idea)
}




class IdeaTableViewCell: UITableViewCell {

    @IBOutlet weak var labelKullaniciAdi: UILabel!
    @IBOutlet weak var labelEklenmeTarihi: UILabel!
    @IBOutlet weak var labelFikirText: UILabel!
    @IBOutlet weak var imageBegeni: UIImageView!
    @IBOutlet weak var labelBegeniSayisi: UILabel!
    @IBOutlet weak var labelYorumSayisi: UILabel!
    @IBOutlet weak var ideaOptionsImageView: UIImageView!
    
    var selectedIdea: Idea!
    var delegate: IdeaTableViewCellDelegate?
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        let tap = UITapGestureRecognizer(target: self, action: #selector(imageBegeniTapped))
        imageBegeni.addGestureRecognizer(tap)
        imageBegeni.isUserInteractionEnabled = true
    }
    
    
    @objc func imageBegeniTapped() {
        Firestore.firestore().collection(Fikirler_REF).document(selectedIdea.documentId).setData([Begeni_Sayisi: selectedIdea.begeniSayisi + 1], merge: true)
    }

    
    func setTableViewCell(idea: Idea, delegate: IdeaTableViewCellDelegate?) {
        selectedIdea = idea
        labelKullaniciAdi.text = idea.kullaniciAdi
        labelFikirText.text = idea.fikirText
        labelBegeniSayisi.text = "\(idea.begeniSayisi ?? 0)"
        
        let tarihFormat = DateFormatter()
        tarihFormat.dateFormat = "dd MM YYYY, hh:mm"
        let eklenmeTarihi = tarihFormat.string(from: idea.eklenmeTarihi)
        labelEklenmeTarihi.text = eklenmeTarihi
        labelYorumSayisi.text = "\(idea.yorumSayisi ?? 0)"
        
        
        ideaOptionsImageView.isHidden = true
        self.delegate = delegate
        
        if idea.kullaniciId == Auth.auth().currentUser?.uid {
            ideaOptionsImageView.isHidden = false
            ideaOptionsImageView.isUserInteractionEnabled = true
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(ideaOptionsImageTapped))
            ideaOptionsImageView.addGestureRecognizer(tap)
        }
        
    }
    
    @objc func ideaOptionsImageTapped() {
        delegate?.ideaOptionsTapped(idea: selectedIdea)
    }

}
