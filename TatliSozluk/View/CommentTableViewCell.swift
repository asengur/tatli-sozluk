//
//  CommentTableViewCell.swift
//  TatliSozluk
//
//  Created by Ali Şengür on 20.07.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import UIKit
import FirebaseAuth


protocol CommentTableViewCellDelegate {
    func commentOptionsImageTapped(comment: Comment)
}



class CommentTableViewCell: UITableViewCell {

    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var commentOptionsImageView: UIImageView!
    
    
    var delegate: CommentTableViewCellDelegate?
    var selectedComment: Comment!
    
     
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }

    
    func setTableViewCell(comment: Comment, delegate: CommentTableViewCellDelegate?) {
        usernameLabel.text = comment.kullaniciAdi
        commentLabel.text = comment.yorumText
        let tarihFormat = DateFormatter()
        tarihFormat.dateFormat = "dd MM YYYY, hh:mm"
        let eklenmeTarihi = tarihFormat.string(from: comment.eklenmeTarihi)
        dateLabel.text = eklenmeTarihi
        
        
        commentOptionsImageView.isHidden = true
        selectedComment = comment
        self.delegate = delegate
        
        if comment.kullaniciId == Auth.auth().currentUser?.uid {
            commentOptionsImageView.isHidden = false
            commentOptionsImageView.isUserInteractionEnabled = true
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(commentOptionsImageTapped))
            commentOptionsImageView.addGestureRecognizer(tap)
        }
    }
    
    @objc func commentOptionsImageTapped() {
        delegate?.commentOptionsImageTapped(comment: selectedComment )
    }
    
}
