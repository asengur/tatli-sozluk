//
//  Categories.swift
//  TatliSozluk
//
//  Created by Ali Şengür on 18.07.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import Foundation



enum Categories: String {
    case Eglence = "Eğlence"
    case Gundem = "Gündem"
    case Populer = "Popüler"
    case Absurt = "Absürt"
}
