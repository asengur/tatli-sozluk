//
//  Comment.swift
//  TatliSozluk
//
//  Created by Ali Şengür on 20.07.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import Foundation
import Firebase

class Comment {
    
    private(set) var kullaniciAdi: String!
    private(set) var eklenmeTarihi: Date!
    private(set) var yorumText: String!
    private(set) var documentId: String!
    private(set) var kullaniciId: String!
    
    
    init(kullaniciAdi: String, eklenmeTarihi: Date, yorumText: String, documentId: String, kullaniciId: String) {
        self.kullaniciAdi = kullaniciAdi
        self.eklenmeTarihi = eklenmeTarihi
        self.yorumText = yorumText
        self.documentId = documentId
        self.kullaniciId = kullaniciId
    }
    
    
    class func getComments(snapshot: QuerySnapshot?) -> [Comment] {
        var comments = [Comment]()
        
        guard let snap = snapshot else { return comments}
         
        for record in snap.documents {
            let data = record.data()
            let kullaniciAdi = data[KULLANICI_ADI] as? String ?? "Misafir"
            let ts = data[Eklenme_Tarihi] as? Timestamp ?? Timestamp()
            let eklenmeTarihi = ts.dateValue()
            let yorumText = data[YORUM_TEXT] as? String ?? "Yorum yok"
            let documentId = record.documentID
            let kullaniciId = data[KULLANICI_ID] as? String ?? ""
            let newComment = Comment(kullaniciAdi: kullaniciAdi, eklenmeTarihi: eklenmeTarihi, yorumText: yorumText, documentId: documentId, kullaniciId: kullaniciId)
            comments.append(newComment)
        }
        
        return comments
    }
    
}
