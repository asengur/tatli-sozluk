//
//  Idea.swift
//  TatliSozluk
//
//  Created by Ali Şengür on 18.07.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import Foundation
import Firebase


class Idea {
    
    private(set) var kullaniciAdi: String!
    private(set) var eklenmeTarihi: Date!
    private(set) var fikirText: String!
    private(set) var yorumSayisi: Int!
    private(set) var begeniSayisi: Int!
    private(set) var documentId: String!
    private(set) var kullaniciId: String!
    
    
    init(kullaniciAdi: String, eklenmeTarihi: Date, fikirText: String, yorumSayisi: Int, begeniSayisi: Int, documentId: String, kullaniciId: String!) {
        self.kullaniciAdi = kullaniciAdi
        self.eklenmeTarihi = eklenmeTarihi
        self.fikirText = fikirText
        self.yorumSayisi = yorumSayisi
        self.begeniSayisi = begeniSayisi
        self.documentId = documentId
        self.kullaniciId = kullaniciId
    }
    
    
    /// Bu fonksiyon, bir "QuerySnapshot" nesnesi alır ve "Idea" türünden bir dizi döndürür.
    class func getIdea(snapshot: QuerySnapshot?) -> [Idea] {
        var ideas = [Idea]() /// Boş bir "ideas" dizisi oluştur.
        guard let snap = snapshot else { return ideas } /// snapshot nesnesi varsa "snap" olarak kullan, yoksa boş olan "ideas" dizisini döndür.
        for document in snap.documents { /// snap içindeki tüm dökümanlar
            let data = document.data() /// döküman içerisindeki bilgiyi "data" değişkenine ata.
            
            let kullaniciAdi = data[Kullanici_Adi] as? String ?? "Misafir"
            let ts = data[Eklenme_Tarihi] as? Timestamp ?? Timestamp()
            let eklenmeTarihi = ts.dateValue()
            let fikirText = data[Fikir_Text] as? String ?? ""
            let yorumSayisi = data[Yorum_Sayisi] as? Int ?? 0
            let begeniSayisi = data[Begeni_Sayisi] as? Int ?? 0
            let documentId = document.documentID
            let kullaniciId = data[KULLANICI_ID] as? String ?? ""
            
            /// snapshot içindeki dökümandan gelen bilgiler ile "yeniFikir" değişkeni oluştur ve bu değişkeni "ideas" dizisine ekle. Bu işlem, snapshot içindeki tün dökümanlar bitene kadar devam edecek, tüm döküman ideas dizisine eklenecek.
            let yeniFikir = Idea(kullaniciAdi: kullaniciAdi, eklenmeTarihi: eklenmeTarihi, fikirText: fikirText, yorumSayisi: yorumSayisi, begeniSayisi: begeniSayisi, documentId: documentId, kullaniciId: kullaniciId)
            ideas.append(yeniFikir)
        }
        return ideas 
    }
    
}
