//
//  Constants.swift
//  TatliSozluk
//
//  Created by Ali Şengür on 18.07.2020.
//  Copyright © 2020 Ali Şengür. All rights reserved.
//

import Foundation



let Fikirler_REF = "Fikirler"
let KATEGORI = "Kategori"
let Begeni_Sayisi = "BegeniSayisi"
let Yorum_Sayisi = "YorumSayisi"
let Fikir_Text = "FikirText"
let Kullanici_Adi = "KullaniciAdi"
let Eklenme_Tarihi = "EklenmeTarihi"


let KULLANICILAR_REF = "Kullanicilar"
let KULLANICI_ADI = "KullaniciAdi"
let KULLANICI_OLUSTURMA_TARIHI = "OlusturulmaTarihi"
let KULLANICI_ID = "KullaniciId"

let YORUMLAR_REF = "Yorumlar"
let YORUM_TEXT = "YorumText"
